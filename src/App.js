// Built-in imports
import { useState } from "react";

// downloaded package imports  
import { Container } from "react-bootstrap";
import { BrowserRouter as Router } from "react-router-dom";
import { Route, Routes } from "react-router-dom";

import './App.css';

import AppNavBar from "./components/AppNavBar";
import Addproduct from "./pages/Addproduct";
import Dashboard from "./pages/Dashboard";
import Error from "./pages/Error";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Updateproduct from "./pages/Updateproduct";
import Welcome from "./pages/Welcome";
import { UserProvider } from "./UserContext";

function App() {

  const [ user, setUser ] = useState({
    id: null,
    isAdmin: null
  });

  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }
  
  return (
    
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavBar/>
        <Container>
          <Routes>
            <Route path="/login" element={<Login/>}/>
            <Route path="/logout" element={<Logout/>}/>
            <Route path="/" element={<Welcome/>}/>
            <Route path="/dashboard" element={<Dashboard/>}/>
            <Route path="/addproduct" element={<Addproduct/>}/>
            <Route path="/updateproduct/:productId" element={<Updateproduct/>}/>
            <Route path="*" element={<Error/>}/>
          </Routes>
        </Container>
      </Router>
    </UserProvider>

  );
}

export default App;
