import { useState } from 'react'
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import Swal from "sweetalert2";

export default function TableData ({ product, id }) {

  const navigate = useNavigate();

  let [isActive, setIsActive] = useState(product.isActive);

  let { _id, name, description, price } = product;

  if(isActive) {
    isActive = "Available";
  } else {
    isActive = "Unavailable";
  }

  function handleDisable() {
    const token = localStorage.getItem('token');
    fetch(`https://capstone2-matunog.onrender.com/api/products/${_id}/archive`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify({
        isActive: false
      }),
    }).then((res) => res.json())
      .then((data) => {
        if (data !== "") {
          setIsActive(false);
          Swal.fire({
            title: "Product Disabled Successfully",
            icon: "success",
            text: "This product wont show in the user App",
          });
          navigate("/dashboard");
        } else {
          Swal.fire({
            title: "Error",
            icon: "error",
            text: "Disabling Product failed. Please try again.",
          });
        }
      });
  }

  function handleEnable() {
    const token = localStorage.getItem('token');
    fetch(`https://capstone2-matunog.onrender.com/api/products/${_id}/activate`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify({
        isActive: true
      }),
    }).then((res) => res.json())
      .then((data) => {
        if (data !== "") {
          setIsActive(true);
          Swal.fire({
            title: "Product Enabled Successfully",
            icon: "success",
            text: "This product will now show in the user App",
          });
          navigate("/dashboard");
        } else {
          Swal.fire({
            title: "Error",
            icon: "error",
            text: "Enabling Product failed. Please try again.",
          });
        }
      });
  }

  return (
    <>
      <tr>
        <th scope="row">{ id + 1 }</th>
        <td>{name}</td>
        <td>{description}</td>
        <td>{price}</td>
        <td>{isActive}</td>
        <td>
          <Button className="mx-1" variant="primary">
            <Link className="text-white text-decoration-none" to={`/updateproduct/${_id}`}>Update</Link>
          </Button>
          {isActive === "Unavailable" ? (
            <Button className="mx-1" variant="success" onClick={handleEnable}>Enable</Button>
          ) : (
            <Button className="mx-1" variant="danger" onClick={handleDisable}>Disable</Button>
          )}
        </td>
      </tr>
    </>
  )
}