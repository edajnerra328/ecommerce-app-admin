import { useContext } from "react";
import { Container, Navbar, Nav } from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";

import UserContext from "../UserContext";

export default function AppNavBar () {

  const { user } = useContext(UserContext);

  return (
    <Navbar bg="primary" expand="lg">
      <Container>
        <Navbar.Brand as={ Link } to="/dashboard" className="text-white">Admin Dashboard</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav"></Navbar.Toggle>
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
            {
              (user.isAdmin) ? 
                <Nav.Link as={ NavLink } to="/logout" className="text-white">Logout</Nav.Link>
                :
                <>
                  <Nav.Link as={ NavLink } to="/login" className="text-white">Login</Nav.Link>
                </>
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}