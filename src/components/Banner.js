import { Link } from "react-router-dom";
import Dashboard from '../pages/Dashboard';

export default function Banner ({isValidRoute}) {
  return (
    <div className='bg-light mt-3 text-dark'>
      {isValidRoute ? (
        <Dashboard />
      ) : (
        <div className="p-5">
          <h1 className='text-center'>Page Not Found</h1>
          <p className='text-center'>Go back to the <Link as={ Link } to="/">Welcome Page.</Link></p>
        </div>
      )}
    </div>   
  )
}