import { useEffect, useState, useContext } from "react";
import { Button } from "react-bootstrap";
import { Link, Navigate } from 'react-router-dom';
import TableData from '../components/TableData';

import UserContext from "../UserContext";

const Dashboard = () => {

  const { user } = useContext(UserContext);

  const [ products, setProducts ] = useState([]);

  useEffect(() => {
    const token = localStorage.getItem('token');
    fetch(`https://capstone2-matunog.onrender.com/api/products/all`, {
      headers: {
        'Authorization': `Bearer ${token}`
      }
    })
      .then(res => res.json())
      .then(data => {   
        setProducts(data);
      })
  }, []);

  return (

    (!user.isAdmin) ?
    <Navigate to={"/login"}/>
    :
    <div className="row welcome">
      <div className="col-12 d-flex justify-content-center align-items-center flex-column ">
        <h1>Admin Dashboard</h1>
        <div>
          <Button className="mx-1" variant="primary">
            <Link className="text-white text-decoration-none" to={"/addproduct"}>Add New Product</Link>
          </Button>
          <Button className="mx-1" variant="success">
            <Link className="text-white text-decoration-none" to={"/addproduct"}>Show User Orders</Link>  
          </Button>  
        </div>
        <table className="table border-dark mt-3 w-75">
          <thead className="bg-dark text-white">
            <tr>
              <th scope="col">Id</th>
              <th scope="col">Name</th>
              <th scope="col">Description</th>
              <th scope="col">Price</th>
              <th scope="col">Availability</th>
              <th scope="col">Actions</th>
            </tr>
          </thead>
          <tbody>
            {products.length > 0 ? (
              products.map((product, index) => (
                <TableData key={product._id} product={product} id={index} />
              ))
            ) : (
              <tr>
                <td colSpan="6">Loading...</td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
    </div>
  )
}

export default Dashboard