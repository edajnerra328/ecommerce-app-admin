
export default function Home () {
  return (
    <>
      <div className="row welcome">
        <div className="col-12 d-flex justify-content-center align-items-center">
          <div className="text-center">
            <h1>Welcome Admin</h1>
            <h3>Ready to work again?</h3>
            <h5>Please Login!</h5>
          </div>
        </div>
      </div>
    </>
  )
}