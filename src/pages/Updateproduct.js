import { useState, useEffect } from 'react';
import { Form, Button, Card, Row, Col } from 'react-bootstrap';
import { useNavigate, useParams } from 'react-router-dom';
import Swal from "sweetalert2";

export default function Updateproduct () {

  const { productId } = useParams();
  const navigate = useNavigate();

  const [updatename, setUpdateName] = useState('');
  const [updatedescription, setUpdateDescription] = useState('');
  const [updateprice, setUpdatePrice] = useState(0);

  function updateproduct (e) {
    e.preventDefault();
    const token = localStorage.getItem('token');
    fetch(`https://capstone2-matunog.onrender.com/api/products/${productId}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify({
        name: updatename,
        description: updatedescription,
        price: updateprice,
      }),
    }).then((res) => res.json())
      .then((data) => {
        if (data !== "") {
          Swal.fire({
            title: "Product Successfully Updated",
            icon: "success",
            text: "Hope you get many orders",
          });
          navigate("/dashboard");
        } else {
          Swal.fire({
            title: "Error",
            icon: "error",
            text: "Update Product failed. Please try again.",
          });
        }
      });
  }

  function handleCancel() {
    navigate("/dashboard");
  }

  useEffect(() => {
    fetch(`https://capstone2-matunog.onrender.com/api/products/${productId}`)
    .then(res => res.json())
    .then(data => {
      setUpdateName(data.name);
      setUpdateDescription(data.description);
      setUpdatePrice(data.price);
    })
  }, [productId]);

  return (
    <Row className="register justify-content-center align-items-center">
      <Col className="d-flex justify-content-center align-items-center">
        <Card className="px-md-4">
          <Card.Body>
            <Form onSubmit={(e) => updateproduct(e)}>
              <h1 className="text-center">Update Product</h1>
              <Form.Group className="mb-3" controlId="name">
                <Form.Label>Name</Form.Label>
                <Form.Control 
                  type="text"
                  placeholder="Update Product Name" 
                  value={updatename ?? ''} 
                  onChange={e => setUpdateName(e.target.value)}
                  required/>
              </Form.Group>

              <Form.Group className="mb-3" controlId="description">
                <Form.Label>Description</Form.Label>
                <Form.Control 
                  type="text"
                  placeholder="Update Product Description" 
                  value={updatedescription ?? ''}  
                  onChange={e => setUpdateDescription(e.target.value)}
                  required/>
              </Form.Group>

              <Form.Group className="mb-3" controlId="price">
                <Form.Label>Price</Form.Label>
                <Form.Control 
                  type="number"
                  placeholder="Update Product Price" 
                  value={ updateprice ?? ''} 
                  onChange={e => setUpdatePrice(e.target.value)} 
                  required/>
              </Form.Group>
              <div className='d-flex justify-content-between align-items-center'>
                <Button variant="primary" type="submit" id="submitBtn">
                  Update Product
                </Button> 
                <Button variant="danger" type="button" id="cancelBtn" onClick={handleCancel}>
                  Cancel Update
                </Button>  
              </div>  
            </Form>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  )
}