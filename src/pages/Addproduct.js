import { useState } from 'react';
import { Form, Button, Card, Row, Col } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Swal from "sweetalert2";

export default function Addproduct () {

  const navigate = useNavigate();

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);

  function addproduct (e) {

    e.preventDefault();
    
    const token = localStorage.getItem('token');
    fetch("https://capstone2-matunog.onrender.com/api/products/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
      }),
    }).then((res) => res.json())
      .then((data) => {

        if (typeof data !== "undefined") {

          Swal.fire({
            title: "Product Successfully Added",
            icon: "success",
            text: "Hope you get many orders",
          });

          navigate("/dashboard");
        } else {
          Swal.fire({
            title: "Error",
            icon: "error",
            text: "Add Product failed. Please try again.",
          });
        }
      });
}

function handleCancel() {
  navigate("/dashboard");
}

  return (
    <Row className="register justify-content-center align-items-center">
      <Col className="d-flex justify-content-center align-items-center">
        <Card className="px-md-4">
          <Card.Body>
            <Form onSubmit={(e) => addproduct(e)}>
              <h1 className="text-center">Add New Product</h1>
              <Form.Group className="mb-3" controlId="name">
                <Form.Label>Name</Form.Label>
                <Form.Control 
                  type="text"
                  placeholder="Enter Product Name" 
                  value={ name } 
                  onChange={e => setName(e.target.value)}
                  required/>
              </Form.Group>

              <Form.Group className="mb-3" controlId="description">
                <Form.Label>Description</Form.Label>
                <Form.Control 
                  type="text"
                  placeholder="Enter Product Description" 
                  value={ description } 
                  onChange={e => setDescription(e.target.value)}
                  required/>
              </Form.Group>

              <Form.Group className="mb-3" controlId="price">
                <Form.Label>Price</Form.Label>
                <Form.Control 
                  type="number"
                  placeholder="Enter Product Price" 
                  value={ price } 
                  onChange={e => setPrice(e.target.value)} 
                  required/>
              </Form.Group>
              <div className='d-flex justify-content-between align-items-center'>
                <Button variant="primary" type="submit" id="submitBtn">
                  Add Product
                </Button> 
                <Button variant="danger" type="button" id="cancelBtn" onClick={handleCancel}>
                  Cancel
                </Button>  
              </div> 
            </Form>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  )
}
